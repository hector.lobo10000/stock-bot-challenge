using Common.CQRS;
using FluentValidation;

namespace Common.Test
{
    class FakeValidator : FluentValidator<FakeCommand>
    {
        public FakeValidator()
        {
            RuleFor(command => command.Name).NotNull();
        }
    }
}