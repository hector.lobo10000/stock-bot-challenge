using Common.CQRS;
using NUnit.Framework;

namespace Common.Test
{
    
    [TestFixture]
    public class FluentValidatorTest
    {


        [Test]
        public void ShouldNotThrowExceptionIfValidationIsCorrect()
        {
            var validator = new FakeValidator();


            FakeCommand fakeCommand = new FakeCommand(){ Name = "fakeName"};
            
            Assert.DoesNotThrowAsync(() => validator.ValidateCommand(fakeCommand) );
        }

        [Test]
        public void ShouldThrowExceptionIfValidationFails()
        {
            var validator = new FakeValidator();


            FakeCommand fakeCommand = new FakeCommand(){ Name = null};
            
            Assert.ThrowsAsync<NotValidException>(() => validator.ValidateCommand(fakeCommand) );
        }
        
    }
}