using Autofac;
using Common.CQRS;
using Moq;
using NUnit.Framework;

namespace Common.Test
{
    [TestFixture]
    public class AutoFacCommandDispatcherTests
    {
        private IDependencyResolver _dependencyResolver;
        private CommandDispatcher _commandDispatcher;
        private FakeCommand _fakeCommand;
        private ICommandHandler<FakeCommand> _commandHandler;
        private ICommandValidator<FakeCommand> _commandValidator;

        [SetUp]
        public void Setup()
        {
            _dependencyResolver = Mock.Of<IDependencyResolver>();
            _commandDispatcher = new CommandDispatcher(_dependencyResolver);
            _fakeCommand = new FakeCommand();
            _commandHandler = Mock.Of<ICommandHandler<FakeCommand>>();
        }

        [Test]
        public void ShouldHandleCommandHandlers()
        {
            ConfigureDependencyResolverToResolveHandler();

            _commandDispatcher.Dispatch(_fakeCommand);


            Mock.Get(_commandHandler).Verify(handler => handler.Handle(_fakeCommand));
        }

        private void ConfigureDependencyResolverToResolveHandler()
        {
            Mock.Get(_dependencyResolver).Setup(resolver => resolver.ResolveCommandHandler(_fakeCommand))
                .Returns(_commandHandler);

            _commandValidator = Mock.Of<ICommandValidator<FakeCommand>>();
            Mock.Get(_dependencyResolver).Setup(r => r.ResolveCommandValidator(_fakeCommand))
                .Returns(_commandValidator);
        }

        [Test]
        public void ShouldCallCommandValidation()
        {
            ConfigureDependencyResolverToResolveHandler();

            _commandDispatcher.Dispatch(_fakeCommand);


            Mock.Get(_commandValidator).Verify(validator => validator.ValidateCommand(_fakeCommand));
        }
    }
}