using Domain.Chat.Entities;
using Domain.Chat.Repositories;

namespace Data
{
    public class ChatRoomRepository : StockBotRepository<ChatRoom> , IChatRoomRepository
    {
        public ChatRoomRepository(StockBotDbContext dbContext) : base(dbContext)
        {
        }
    }
}