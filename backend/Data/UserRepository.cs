using Domain.Users;
using Domain.Users.Entities;

namespace Data
{
    public class UserRepository : StockBotRepository<User>, IUserRepository
    {
        public UserRepository(StockBotDbContext dbContext) : base(dbContext)
        {
        }
    }
}