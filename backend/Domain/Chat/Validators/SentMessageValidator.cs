using Common.CQRS;
using Domain.Chat.Commands;
using FluentValidation;

namespace Domain.Chat.Validators
{
    public class SentMessageValidator : FluentValidator<SendMessage>
    {

        public SentMessageValidator()
        {
            RuleFor(message => message.Content).NotNull();
            RuleFor(message => message.ChatId).GreaterThan(0);
            RuleFor(message => message.UserId).GreaterThan(0);
        }
        
    }
}