using System.Threading.Tasks;
using Common.CQRS;
using Domain.Chat.Commands;
using Domain.Chat.Entities;
using Domain.Chat.Repositories;

namespace Domain.Chat.Handlers
{
    public class ChatRoomCreator :  ICommandHandler<CreateChatRoom>

    {
        private readonly IChatRoomRepository _repository;

        public ChatRoomCreator(IChatRoomRepository repository)
        {
            _repository = repository;
        }
        public Task Handle(CreateChatRoom command)
        {
            var chat = new ChatRoom()
            {
                Name = command.Name,
            };


            return _repository.Create(chat);
        }
    }
}