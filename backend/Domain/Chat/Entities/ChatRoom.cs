using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Entities;
using Domain.Users.Entities;

namespace Domain.Chat.Entities
{
    [Table("ChatRoom")]
    public class ChatRoom : IEntity
    {
        [Key] public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<Message> Messageses { get; set; } = new List<Message>();

        public void AddMessage(Message message)
        {
            message.Send(DateTime.Now);
            Messageses.Add(message);
        }
    }
}