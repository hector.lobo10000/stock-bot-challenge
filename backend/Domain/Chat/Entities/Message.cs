using System;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Entities;
using Domain.Users.Entities;

namespace Domain.Chat.Entities
{
    [Table("Message")]
    public class Message : IEntity
    {
        public Message()
        {
            
        }
        public Message(string content, User owner)
        {
            Content = content;
            Owner = owner;
        }

        public int Id { get; set; }
        
        public string Content { get; set; }
        
        public DateTime SentDate { get; set; }

        public User Owner { get; set; }
        public int ChatRoomId { get; set; }

        public void Send(DateTime sentDate)
        {
            SentDate = sentDate;

        }
    }
}