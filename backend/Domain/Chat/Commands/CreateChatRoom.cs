using Common.CQRS;

namespace Domain.Chat.Commands
{
    public class CreateChatRoom : ICommand
    {
        public CreateChatRoom(string name)
        {
            this.Name = name;
        }

        public string Name { get; set; }
    }
}