using System.Linq;
using System.Threading.Tasks;
using Common;
using Common.CQRS;
using Domain.Chat.Events;
using Domain.Chat.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Domain.Chat.EventHandlers
{
    public class NotifyMessageRealTime : IEventHandler<MessageSent>
    {
        private readonly INotificationService _notificationService;
        private readonly IMessageRepository _repository;

        public NotifyMessageRealTime(INotificationService notificationService, IMessageRepository repository)
        {
            _notificationService = notificationService;
            _repository = repository;
        }
        public async Task Handle(MessageSent e)
        {
            var message = await _repository.GetAll()
                .Where(room => room.Id == e.MessageId)
                .Include(room => room.Owner)
                .FirstAsync();

           
            
           await _notificationService.NotifyMessage(new MesssagePayload()
           {
               ChatId =e.ChatId, 
               MessageId = e.MessageId,
               Content = message.Content,
               Owner = message.Owner.UserName,
               Date = message.SentDate
           });
        }
    }
}