using System.Threading.Tasks;
using Domain.Chat.Entities;

namespace Domain.Bot
{
    public interface IBotCommandStrategy
    {
        public string Command { get; }
        bool CanHandle(Message message);

        Task Reply(Message message);
    }
}