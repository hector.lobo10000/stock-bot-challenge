using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Chat.Entities;

namespace Domain.Bot
{
    public interface IBotQueue
    {
        Task SendToQueue(IBotCommandStrategy strategy, Message message);
    }
}