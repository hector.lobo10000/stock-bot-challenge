using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Domain.Bot.BotStrategies;
using Domain.Chat.Entities;

namespace Domain.Bot
{
    public class Bot : IBot
    {
        private readonly IEnumerable<IBotCommandStrategy> _strategies;
        private readonly IBotQueue _botQueue;

        public Bot(IEnumerable<IBotCommandStrategy> strategies, IBotQueue botQueue)
        {
            _strategies = strategies;
            _botQueue = botQueue;
        }

        public async Task ProcessMessage(Message message)
        {
            var botCommandStrategies = _strategies.Where(strategy => strategy.CanHandle(message)).ToList();


            if (message.Content.StartsWith("/") && botCommandStrategies.Count == 0)
            {
                botCommandStrategies.AddRange(_strategies.Where(strategy => strategy.Command.ToLower() == "error"));
            }


            var tasks = botCommandStrategies
                .Select(strategy => QueueBotCommand(strategy, message))
                .ToArray();


            await Task.WhenAll(tasks);
        }

        public async Task ProcessMessage(string command, Message message)
        {
            var strategy = _strategies.FirstOrDefault(s => s.Command == command);

            if (strategy != null) await strategy.Reply(message);
        }

        private  Task QueueBotCommand(IBotCommandStrategy strategy, Message message)
        { 
            return _botQueue.SendToQueue(strategy, message);
        }
    }
}