using System;
using System.Security.Cryptography;
using Domain.Users.Entities;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.AspNetCore.Identity;

namespace Domain.Users.Services
{
    public interface IPasswordGenerator
    {
        string HashPassword(User user, string password);
        bool VerifyPassword(User user, string password, string receivedPassword);
    }

    public class PasswordGenerator : IPasswordGenerator
    {
        public string HashPassword(User user, string password)
        {
            var hasher = new PasswordHasher<User>();

            return hasher.HashPassword(user, password);
        }

        public bool VerifyPassword(User user, string password, string receivedPassword)
        {
            var hasher = new PasswordHasher<User>();
            hasher.VerifyHashedPassword(user, user.Password, "dkdk");

            var passwordVerificationResult = hasher.VerifyHashedPassword(user, password, receivedPassword);

            if (passwordVerificationResult == PasswordVerificationResult.SuccessRehashNeeded)
            {
                return true;
            }
            
            return passwordVerificationResult == PasswordVerificationResult.Success;
        }
    }
}