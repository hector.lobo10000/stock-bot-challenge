using Common.CQRS;
using Domain.Chat.Commands;
using Domain.Chat.Handlers;
using Domain.Chat.Repositories;
using Domain.Users;
using Domain.Users.Entities;
using Moq;
using NUnit.Framework;

namespace Domain.Test.ChatRoom
{
    [TestFixture]
    public class SendMessageTest
    {
        [Test]
        public void ShouldSendMessageWithData()
        {
            var chatRoomRepository = Mock.Of<IChatRoomRepository>();
            var userRepository = Mock.Of<IUserRepository>();

            var eventDispatcher = Mock.Of<IEventDispatcher>();    
            var sender = new MesssageSender(chatRoomRepository, userRepository, eventDispatcher);

            User user = new User("fake", "fake");
            var chat = new Chat.Entities.ChatRoom();
            var sendMessage = new SendMessage();

            Mock.Get(chatRoomRepository).Setup(repository => repository.GetById(sendMessage.ChatId)).ReturnsAsync(chat);
            Mock.Get(userRepository).Setup(repository => repository.GetById(sendMessage.ChatId)).ReturnsAsync(user);


            sender.Handle(sendMessage);


            Mock.Get(chatRoomRepository)
                .Verify(repository =>
                    repository.Update(It.Is<Chat.Entities.ChatRoom>(room => room.Equals(chat))));
        }
    }
}