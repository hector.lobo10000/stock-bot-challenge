using Domain.Users;
using Domain.Users.Commands;
using Domain.Users.Entities;
using Domain.Users.Handlers;
using Domain.Users.Services;
using Moq;
using NUnit.Framework;

namespace Domain.Test.Users
{
    [TestFixture]
    public class UserCreatorTest
    {
        [Test]
        public void ShouldHashPassword()
        {
            var userRepository = Mock.Of<IUserRepository>();
            var passwordGenerator = Mock.Of<IPasswordGenerator>();

            var creator = new UserCreator(userRepository, passwordGenerator);

            string hassedPassword = "fakehashedPassword";
            var registerUser = new RegisterUser("fakeUsername", "fakepass", "fake", "fake Name");


            Mock.Get(passwordGenerator)
                .Setup(generator => generator.HashPassword(It.IsAny<User>(), registerUser.Password))
                .Returns(hassedPassword);
            creator.Handle(registerUser);


            Mock.Get(userRepository)
                .Verify(repository => repository.Create(It.Is<User>(user => user.Password == hassedPassword)));
        }
    }
}