using System.Threading.Tasks;
using Domain.Bot;
using Domain.Chat.Entities;
using MassTransit;

namespace Infrastructure.RabbitMqInfra
{
    public class RabbitQueueBot : IBotQueue
    {
        private readonly IBus _bus;

        public RabbitQueueBot(IBus bus)
        {
            _bus = bus;
        }
        public async Task SendToQueue(IBotCommandStrategy strategy, Message message)
        {
           await _bus.Publish(new RunBot()
            {
                Strategy = strategy.Command, MessageId = message.Id
            });
        }
    }
}