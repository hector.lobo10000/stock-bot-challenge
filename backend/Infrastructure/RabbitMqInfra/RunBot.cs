namespace Infrastructure.RabbitMqInfra
{
    public class RunBot
    {
        public string Strategy { get; set; }

        public int MessageId { get; set; }
    }
}