using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using CsvHelper;
using Domain.Bot.BotStrategies;
using RestSharp;

namespace Infrastructure
{
    public class SockApi : ISockApi
    {
        public async Task<StockModel> GetStock(string stockCode)
        {
                    var client = new RestClient();

            var getRsult = 
                client.Get(new RestRequest($"https://stooq.com/q/l/?s={stockCode.ToLower()}&f=sd2t2ohlcv&h&e=csv"));

            if (getRsult.StatusCode != HttpStatusCode.OK)
            {
                return null;
            }

            var stringReader = new StringReader(getRsult.Content);
            using var csv = new CsvReader(stringReader, CultureInfo.InvariantCulture);
            
            var data = csv.GetRecords<StockModel>();

            return data.FirstOrDefault();
        }
    }
}