using Autofac;
using Autofac.Core;
using Autofac.Core.Registration;
using Common.CQRS;
using Data;
using Domain.Users.Entities;
using Infrastructure;
using WebApplication.Controllers.ChatRoomWeb;

namespace WebApplication
{
    public class WebAppModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(typeof(ICommand).Assembly).AsImplementedInterfaces();
            builder.RegisterAssemblyTypes(typeof(UserRepository).Assembly).AsImplementedInterfaces();
            builder.RegisterAssemblyTypes(typeof(User).Assembly).AsImplementedInterfaces();
            builder.RegisterAssemblyTypes(typeof(AutofacDependencyResolver).Assembly).AsImplementedInterfaces();
            builder.RegisterType<SignalRNotificationService>().AsImplementedInterfaces();
            
            base.Load(builder);
        }
    }
}