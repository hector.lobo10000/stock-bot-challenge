using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common.CQRS;
using Domain.Chat.Commands;
using Domain.Chat.Entities;
using Domain.Chat.Repositories;
using Domain.Users.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace WebApplication.Controllers.ChatRoomWeb
{
    
    [ApiController]
    [Route("api/chats")]
    public class ChatRoomController : ControllerBase
    {
        private readonly ICommandDispatcher _dispatcher;
        private readonly IChatRoomRepository _repository;

        public ChatRoomController(ICommandDispatcher dispatcher, IChatRoomRepository repository)
        {
            _dispatcher = dispatcher;
            _repository = repository;
        }

        [HttpPost]
        public async Task<IActionResult> CreatChatRoom(ChatRoomRequest request)
        {

            await _dispatcher.Dispatch(new CreateChatRoom(request.Name));

            return Accepted();
        }

        [HttpGet]
        public async Task<List<ChatRoomResponse>> GetChats()
        {
            var chats = await _repository.GetAll().ToListAsync();
            
            
            return chats.Select(room => new ChatRoomResponse{Id= room.Id, Name = room.Name}).ToList();
        }

        [HttpGet("{chatId}/messages")]
        public async Task<List<MessageResponse>> GetMessages(int chatId)
        {
            var messages = await _repository.GetAll()
                .Where(room => room.Id == chatId)
                .SelectMany(room => room.Messageses)
                .Include(message => message.Owner)
                .OrderByDescending(message => message.SentDate)
                .Take(50)
                
                .ToListAsync();

            return messages.Select(message => new MessageResponse()
            {
                Content = message.Content,
                Id = message.Id,
                Owner = message.Owner.UserName,
                SentDate = message.SentDate,
                OwnerId = message.Owner.Id
            }).ToList();
        }

        [HttpPost("{chatId}/messages")]
        public async Task<IActionResult> PostMessaege(MessageRequest request)
        {
            await _dispatcher.Dispatch(new SendMessage()
            {
                Content = request.Content,
                ChatId = request.ChatId,
                UserId = request.UserId
                
            });

            return Ok();
        }
    }
}