    namespace WebApplication.Controllers.ChatRoomWeb
{
    public class MessageRequest
    {
        public int ChatId { get; set; }
        public int UserId { get; set; }
        public string Content { get; set; }
    }
}