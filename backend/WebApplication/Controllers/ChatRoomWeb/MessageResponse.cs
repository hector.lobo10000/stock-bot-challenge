using System;

namespace WebApplication.Controllers.ChatRoomWeb
{
    public class MessageResponse
    {
        public int Id { get; set; }
        
        public string Content { get; set; }
        
        public DateTime SentDate { get; set; }

        public string Owner { get; set; }
        public int OwnerId { get; set; }

    }
}