using System.Threading.Tasks;
using Common;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;

namespace WebApplication.Controllers.ChatRoomWeb
{
    public class SignalRNotificationService : INotificationService
    {
        private readonly IHubContext<ChatWebSocket, IChatWebSocket> _context;

        public SignalRNotificationService(IHubContext<ChatWebSocket, IChatWebSocket> context)
        {
            _context = context;
        }

        public async Task NotifyMessage(MesssagePayload message)
        {
            await _context.Clients.All.ReceiveMessage(JsonConvert.SerializeObject(message));
        }

        
    }
}