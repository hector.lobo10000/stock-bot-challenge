using System.Reflection;
using Autofac;
using Infrastructure.RabbitMqInfra;
using MassTransit;
using Module = Autofac.Module;

namespace WebApplication
{
    public class RabbitMqModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.AddMassTransit(x =>
            {
                // add all consumers in the specified assembly
                x.AddConsumers(typeof(RunBotConsumer).Assembly);


                // add the bus to the container
                x.AddBus(context => Bus.Factory.CreateUsingRabbitMq(cfg =>
                {
                    cfg.Host(System.Environment.GetEnvironmentVariable("RABBITMQ_CONNECTION"));

                    cfg.ReceiveEndpoint("run-bot", ec =>
                    {
                        // Configure a single consumer
                        ec.ConfigureConsumer<RunBotConsumer>(context);
                    });

                    // or, configure the endpoints by convention
                    cfg.ConfigureEndpoints(context);
                }));
            });

            base.Load(builder);
        }
    }
}