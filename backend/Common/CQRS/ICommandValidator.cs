using System.Threading.Tasks;

namespace Common.CQRS
{
    public interface ICommandValidator<TCommand> where TCommand : ICommand
    {
        Task ValidateCommand(TCommand command);
    }
}