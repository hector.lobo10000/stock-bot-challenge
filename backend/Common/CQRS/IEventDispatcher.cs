using System.Threading.Tasks;

namespace Common.CQRS
{
    public interface IEventDispatcher
    {
        Task Dispatch<TEvent>(TEvent e) where TEvent : IEvent;
    }
}