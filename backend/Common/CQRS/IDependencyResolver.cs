using System.Collections.Generic;

namespace Common.CQRS
{
    public interface IDependencyResolver
    {
        ICommandHandler<TCommand> ResolveCommandHandler<TCommand>(TCommand command) where TCommand : ICommand;
        ICommandValidator<TCommand> ResolveCommandValidator<TCommand>(TCommand command) where TCommand: ICommand;
        IEnumerable<IEventHandler<TEvent>> ResolverEventHandlers<TEvent>(TEvent @event) where TEvent : IEvent;
    }
}