namespace Common.CQRS
{
    public class ValidationError
    {
        public string Field { get; set; }   
        public string ErrorMessage { get; set; }
        public object Value { get; set; }
    }
}