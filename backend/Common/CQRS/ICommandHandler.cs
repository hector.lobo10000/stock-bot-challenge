

using System.Threading.Tasks;

namespace Common.CQRS
{
    public interface ICommandHandler<TCommand>  where TCommand : ICommand
    {

        Task Handle(TCommand command);
    }
}