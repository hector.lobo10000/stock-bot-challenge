using System.Linq;
using System.Threading.Tasks;
using FluentValidation;

namespace Common.CQRS
{
    public abstract class FluentValidator<TCommand> : AbstractValidator<TCommand>, ICommandValidator<TCommand> where TCommand : ICommand
    {
        public async Task ValidateCommand(TCommand command)
        {
            var result =  await ValidateAsync(command);

            if (result.IsValid) return;
            
            var errors = result.Errors.Select(
              failure => new ValidationError()
              {
                  Field = failure.PropertyName,
                  ErrorMessage = failure.ErrorMessage,
                  Value = failure.AttemptedValue
                  
              }    
            );    
            throw new NotValidException(errors, command);

        }
    }
}