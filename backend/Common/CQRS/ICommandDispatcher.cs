﻿using System.Threading.Tasks;

namespace Common.CQRS
{
    public interface ICommandDispatcher
    {
        Task Dispatch<TCommand>(TCommand command) where TCommand : ICommand;
    }
}