using System;
using System.Collections.Generic;

namespace Common.CQRS
{
    public class NotValidException : Exception
    {
        public IEnumerable<ValidationError> Errors { get; }

        public NotValidException(IEnumerable<ValidationError> errors, ICommand command)
            : base($"Validations Error in Command ${command.GetType()}")
        {
            Errors = errors;
        }
    }
}