import {Container, Button, Form, Row, Col, Alert} from "react-bootstrap";
import React from "react";
import * as signalR from "@microsoft/signalr";


type Chat = {
    messages: any[],
    message: string,

}

const api = "http://localhost:8001";


class Home extends React.Component<{}, Chat> {
    handleChange(e: any){

        this.setState({message: e.target.value});

    }

    async handleSubmit(e: any) {
        console.log('lllamado');
        e.preventDefault();
        try {
            const request = await  fetch(api + "/api/chats/1/messages", {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify( {
                    'chatId': 1,
                    "userId": 1,
                    "content": this.state.message,
                }),
            })

            await request.text();

            await this.setState({message: ''});
        } catch (e) {
            console.log(e)
        }


    }

    constructor(props: any) {
        super(props);

        this.handleChange =this.handleChange.bind(this);
        this.handleSubmit =this.handleSubmit.bind(this);
        this.state={
            messages :[],
            message: '',
        }

        this.startChat();



    }
    componentDidMount(): void {

        const connection = new signalR.HubConnectionBuilder()
            .withUrl(api + "/chatsocket")
            .build();

        this.runServer();

        connection.on("ReceiveMessage", (message: string) => {

            console.log(message);
            const currentMesssage = this.state.messages;
            currentMesssage.push(JSON.parse(message));
            this.setState({ messages: [...currentMesssage]})
        });


        connection.start().catch(err => console.log(err));
    }

    renderMessaage = () =>{
        return <Col>
            {
                this.state.messages.sort((a,b)=>{
                    return new Date(a.scheduled_for).getTime() -
                        new Date(b.scheduled_for).getTime()
                }).reverse().map((m, idx) => (
                <Alert key={idx} variant="info">
                    <Alert.Heading>From {m.Owner} </Alert.Heading>
                    <p> {m.Content}</p>
                    <hr />
                    <p className="mb-0">
                        Sent At {m.Date}
                    </p>
                </Alert>
                ))
            }
        </Col>
    }
    render = () => {
        return <Container>
                <Row>
                    <Col>
                        <Form>
                            <Form.Group controlId="id" onSubmit={this.handleSubmit}>
                                <Form.Label>Message to Send</Form.Label>
                                <Form.Control type="text" value ={this.state.message} onChange={this.handleChange} placeholder="Send a message"/>

                            </Form.Group>
                            <Button variant="primary" onClick={this.handleSubmit}>
                                Submit
                            </Button>
                        </Form>
                    </Col>

                </Row>
                <Row>
                    {this.renderMessaage()}

                </Row>
        </Container>
    };


    async  runServer() {
        const r =await fetch(api + "/api/users");
        const user = await r.json();
    }

    async startChat() {
        try {
            const request = await fetch(api + "/api/chats", {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    "name": "TestChat"
                }),
            })

            await request.text();

            const r = await fetch(api + "/api/users", {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    "userName": "UserForTest",
                    "fullName": "TestUser",
                    "password": "Password123",
                    "confirmPassword": "Password123"
                }),
            });

            await r.text();


        } catch (e) {
            
        }

    }
}

export default Home;