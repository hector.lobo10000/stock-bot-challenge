import React from 'react';
import { Switch, Route, BrowserRouter as Router} from 'react-router-dom';
import logo from './logo.svg';

import './App.css';

import Home from './Home'

function App() {
  return (
    <Router >

      <Switch>
        <Route  path="/" exact component={Home} />
      </Switch>
    </Router>
  );
}

export default App;
