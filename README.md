
#Requirements

Docker
Docker-Compose
Yarn

#To run the app run
Commands
- yarn

- yarn start

Backend http://localhost:8001/
Frontend http://localhost:3000/

#Implentations 

- Simple CQRS implentation with a dispatcher in memory for the CRUD
- SignalR implentation for real time chat
- Docker image for deployments and instalation
- Masstransit for Rabbitmq helper
- User domain with commands and validtors
- Chat domain with ChatRoom and Messages,
- Bot domain with decopuled startegies for handling diferent commands.

